package entity;

public class EasyFugaStrategy implements NPCstrategy {

    public static EasyFugaStrategy instance = new EasyFugaStrategy();

    @Override
    public void action(String direction, Entity entity, Entity playerReference) {
        int xDiferrence = entity.worldX - playerReference.worldX;
        int yDiferrence = entity.worldY - playerReference.worldY;

        if (playerReference.direction.equals("up") | playerReference.direction.equals("down")) {
            if (yDiferrence > 0)
                entity.setDirection("down");
            else
                entity.setDirection("up");
        } else {
            if (xDiferrence > 0)
                entity.setDirection("right");
            else
                entity.setDirection("left");
        }
    }

    @Override
    public int getSpeed() {
        return 3;
    }

}
