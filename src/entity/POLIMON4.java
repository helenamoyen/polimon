package entity;

import main.GamePanel;

public class POLIMON4 extends Entity {

    public POLIMON4(GamePanel gp) {
        super(gp);
        direction = "down";
        speed = 1;
        easyStrategy = new walk2NPC();
        hardStrategy = new walkNPC();
        state = EasyState.instance;
        battle = true;
        energia = 4;

    }

    public void getImage() {

        up1 = setup("./src/res/polimon/eeve.png");
        up2 = setup("./src/res/polimon/eeve.png");
        down1 = setup("./src/res/polimon/eeve.png");
        down2 = setup("./src/res/polimon/eeve.png");
        left1 = setup("./src/res/polimon/eeve.png");
        left2 = setup("./src/res/polimon/eeve.png");
        right1 = setup("./src/res/polimon/eeve.png");
        right2 = setup("./src/res/polimon/eeve.png");

    }

    public void actionNPC() {
        getImage();
        action();
        setDialogue();
    }

    public void setDialogue() {

        dialogues[0] = "Meu deus! Um Mestre POLIMÓN!";
        dialogues[1] = "Tente me capturar se puder!";
        dialogues[2] = "Nunca vai conseguir!";

    }

    public void speak() {

        if (dialogueIndex == 1) {
            easyStrategy = EasyFugaStrategy.instance;
            hardStrategy = HardFugaStrategy.instance;
        }

        gp.sMessages.currentMessage = dialogues[dialogueIndex];
        dialogueIndex++;
        if (dialogueIndex == 3)
            dialogueIndex--;

        // Para colocar o NPC de frente para o jogador:
        switch (gp.player.direction) {

            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "right":
                direction = "left";
                break;
            case "left":
                direction = "right";
                break;
        }

    }

}
