package entity;

import main.GamePanel;

public class NPC_corredor extends Entity {

    public NPC_corredor(GamePanel gp) {
        super(gp);
        energia = 10;
        direction = "down";
        easyStrategy = new walk2NPC();
        hardStrategy = new walkNPC();
        state = EasyState.instance;
        battle = false;
    }

    public void setDialogue() {

        dialogues[0] = "Está perdido politécnico?";
        dialogues[1] = "Deseja chegar à poli?";
        dialogues[2] = "Como pegar o circular? Bom, basta achar um Busp!";
        dialogues[3] = "Não está conseguindo achar?";
        dialogues[4] = "Procure perto de um beco, entre alguns lixos...";
    }

    public void speak() {

        if (dialogues[dialogueIndex] == null) {
            dialogueIndex = 0;
        }

        gp.sMessages.currentMessage = dialogues[dialogueIndex];
        dialogueIndex++;

        // Para colocar o NPC de frente para o jogador:
        switch (gp.player.direction) {

            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "right":
                direction = "left";
                break;
            case "left":
                direction = "right";
                break;
        }

    }

    public void getImage() {

        up1 = setup("./src/res/NPC_2/npc2_up1.png");
        up2 = setup("./src/res/NPC_2/npc2_up2.png");
        down1 = setup("./src/res/NPC_2/npc2_down1.png");
        down2 = setup("./src/res/NPC_2/npc2_down2.png");
        left1 = setup("./src/res/NPC_2/npc2_left1.png");
        left2 = setup("./src/res/NPC_2/npc2_left2.png");
        right1 = setup("./src/res/NPC_2/npc2_right1.png");
        right2 = setup("./src/res/NPC_2/npc2_right2.png");

    }

    public void actionNPC() {
        getImage();
        action();
        setDialogue();
    }

}