package entity;

public class walk2NPC implements NPCstrategy {

	public int actionLockCounter;
	public static walk2NPC instance = new walk2NPC();

	public walk2NPC() {
	}

	@Override
	public void action(String direction, Entity entity, Entity playerReference) {
		actionLockCounter++;
		if (actionLockCounter == 200) {

			if (direction == "up") {
				entity.setDirection("down");
			} else if (direction == "down") {
				entity.setDirection("up");
			}

			actionLockCounter = 0;
		}
	}

	@Override
	public int getSpeed() {
		return 5;
	}

}
