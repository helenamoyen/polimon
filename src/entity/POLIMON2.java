package entity;

import main.GamePanel;

public class POLIMON2 extends Entity {

    public POLIMON2(GamePanel gp) {
        super(gp);
        direction = "down";
        speed = 1;
        easyStrategy = new walk2NPC();
        hardStrategy = new walkNPC();
        state = EasyState.instance;
        battle = true;
        energia = 5;

    }

    public void getImage() {

        up1 = setup("./src/res/polimon/pikachu.png");
        up2 = setup("./src/res/polimon/pikachu.png");
        down1 = setup("./src/res/polimon/pikachu.png");
        down2 = setup("./src/res/polimon/pikachu.png");
        left1 = setup("./src/res/polimon/pikachu.png");
        left2 = setup("./src/res/polimon/pikachu.png");
        right1 = setup("./src/res/polimon/pikachu.png");
        right2 = setup("./src/res/polimon/pikachu.png");

    }

    public void actionNPC() {
        getImage();
        action();
        setDialogue();
    }

    public void setDialogue() {

        dialogues[0] = "Meu deus! Um Mestre POLIMÓN!";
        dialogues[1] = "Tente me capturar se puder!";
        dialogues[2] = "Nunca vai conseguir!";

    }

    public void speak() {

        if (dialogueIndex == 1) {
            easyStrategy = EasyFugaStrategy.instance;
            hardStrategy = HardFugaStrategy.instance;
        }

        gp.sMessages.currentMessage = dialogues[dialogueIndex];
        dialogueIndex++;
        if (dialogueIndex == 3)
            dialogueIndex--;

        // Para colocar o NPC de frente para o jogador:
        switch (gp.player.direction) {

            case "up":
                direction = "down";
                break;
            case "down":
                direction = "up";
                break;
            case "right":
                direction = "left";
                break;
            case "left":
                direction = "right";
                break;
        }

    }

}
