package entity;

public class HardState implements State {

    public static State instance = new HardState();

    @Override
    public State getState() {
        return instance;
    }

    @Override
    public void changeState(Entity entity) {
        entity.state = EasyState.instance;
    }

    @Override
    public NPCstrategy getStrategy(Entity entity) {
        return entity.hardStrategy;
    }

}
