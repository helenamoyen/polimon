package entity;

public interface NPCstrategy {

	public void action(String direction, Entity entity, Entity playerReference);

	public int getSpeed();

}
