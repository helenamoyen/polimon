package tiles;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.awt.RenderingHints;

import javax.imageio.ImageIO;

import main.GamePanel;
import main.KeyHandler;

public class TileManager {
    private GamePanel gp;
    private KeyHandler kh;
    public Tile[] tiles;
    public int[][][] mapTileNum;
    private Map<Integer, BufferedImage> tileset;
    String mapSkin;

    public TileManager(GamePanel gp, KeyHandler kh) {
        this.gp = gp;
        this.kh = kh;
        this.tiles = new Tile[43000];
        this.mapTileNum = new int[gp.maxWorldCol][gp.maxWorldRow][gp.maxMap];
        this.tileset = new HashMap<>();

        getTileImage();

        loadMapLayers(new String[] { "/res/mapas/butantamap_solo.csv", "/res/mapas/butantamap_colisao.csv",
                "/res/mapas/butantamap_colisao.csv" }, 0);
        loadMapLayers(new String[] { "/res/mapas/raiamap_solo.csv", "/res/mapas/raiamap_agua.csv",
                "/res/mapas/raiamap_colisao.csv" }, 1);

    }

    public void getTileImage() {
        try {

            mapSkin = SpritesManager.getFolder() + SpritesManager.getSpritePath();

            InputStream is = getClass().getResourceAsStream(mapSkin);
            BufferedImage tileset = ImageIO.read(is);

            int tileSize = 16; // Tamanho de cada tile
            int tileCount = tiles.length;

            for (int i = 0; i < tileCount; i++) {
                int tileX = (i % (tileset.getWidth() / tileSize)) * tileSize;
                int tileY = (i / (tileset.getWidth() / tileSize)) * tileSize;

                tiles[i] = new Tile();
                tiles[i].image = tileset.getSubimage(tileX, tileY, tileSize, tileSize);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadMapLayers(String[] layerFiles, int map) {
        for (int layerIndex = 0; layerIndex < layerFiles.length; layerIndex++) {
            String filePath = layerFiles[layerIndex];
            loadLayer(filePath, map, layerIndex);
        }
    }

    private void loadLayer(String filePath, int map, int layerIndex) {
        try {
            InputStream is = getClass().getResourceAsStream(filePath);
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            int collisionLayerIndex = 2;
            int waterLayerIndex = 1;
            String line;
            int row = 0;

            while ((line = br.readLine()) != null && row < gp.maxWorldRow) {
                String[] numbers = line.split(",");
                int col = 0;

                for (String number : numbers) {
                    int num = Integer.parseInt(number.trim());

                    if (num != -1) {
                        mapTileNum[row][col][map] = num;

                        // Define o atributo de colisão com base no índice da camada
                        if (layerIndex == collisionLayerIndex) {
                            tiles[num].collision = true;
                        } else {
                            tiles[num].collision = false;
                        }

                        if (layerIndex == waterLayerIndex) {
                            tiles[num].water = true;
                        } else {
                            tiles[num].water = false;
                        }
                    }

                    col++;

                    if (col >= gp.maxWorldCol) {
                        break;
                    }
                }

                row++;
            }

            br.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateSkinMap() {
        if (kh.mapPressed && kh.mapSwitch) {
            getTileImage();
            kh.mapSwitch = false;
        }
    }

    public void desenhar(Graphics2D g2) {
        // Desenhar camada das ruas (sem colisões)
        int worldCol = 0;
        int worldRow = 0;

        while (worldCol < gp.maxWorldCol && worldRow < gp.maxWorldRow) {
            int worldX = worldCol * gp.tileSize;
            int worldY = worldRow * gp.tileSize;
            int screenX = worldX - gp.player.worldX + gp.player.screenX;
            int screenY = worldY - gp.player.worldY + gp.player.screenY;

            if (worldX + gp.tileSize > (gp.player.worldX - gp.player.screenX)
                    && worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
                    worldY + gp.tileSize > (gp.player.worldY - gp.player.screenY)
                    && worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {
                int tileIndex = mapTileNum[worldRow][worldCol][gp.currentMap];

                if (!tiles[tileIndex].collision) {
                    g2.drawImage(tiles[tileIndex].image, screenX, screenY, gp.tileSize, gp.tileSize, null);
                }
            }

            worldCol++;
            if (worldCol == gp.maxWorldCol) {
                worldCol = 0;
                worldRow++;
            }
        }

        // Desenhar camada das construções (com colisões)
        worldCol = 0;
        worldRow = 0;

        while (worldCol < gp.maxWorldCol && worldRow < gp.maxWorldRow) {
            int worldX = worldCol * gp.tileSize;
            int worldY = worldRow * gp.tileSize;
            int screenX = worldX - gp.player.worldX + gp.player.screenX;
            int screenY = worldY - gp.player.worldY + gp.player.screenY;

            if (worldX + gp.tileSize > (gp.player.worldX - gp.player.screenX)
                    && worldX - gp.tileSize < gp.player.worldX + gp.player.screenX &&
                    worldY + gp.tileSize > (gp.player.worldY - gp.player.screenY)
                    && worldY - gp.tileSize < gp.player.worldY + gp.player.screenY) {
                int tileIndex = mapTileNum[worldRow][worldCol][gp.currentMap];

                if (tiles[tileIndex].collision) {
                    if (tileIndex != -1) {
                        g2.drawImage(tiles[tileIndex].image, screenX, screenY, gp.tileSize, gp.tileSize, null);
                    } else {
                        int streetTileIndex = mapTileNum[worldRow][worldCol][gp.currentMap - 1];
                        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                        g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                                RenderingHints.VALUE_INTERPOLATION_BICUBIC);
                        g2.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
                        g2.drawImage(tiles[streetTileIndex].image, screenX, screenY, gp.tileSize, gp.tileSize, null);
                    }
                }
            }

            worldCol++;
            if (worldCol == gp.maxWorldCol) {
                worldCol = 0;
                worldRow++;
            }
        }
    }
}
