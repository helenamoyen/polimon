package object;

import java.io.IOException;
import javax.imageio.ImageIO;

public class Catraca extends SuperObject {
    
    public Catraca() {
        name = "catraca";
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/res/objects/catraca.png"));

        }catch(IOException e) {
            e.printStackTrace();
        }
        collision = true;
    }
}
