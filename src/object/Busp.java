package object;

import java.io.IOException;
import javax.imageio.ImageIO;

public class Busp extends SuperObject {
    
    public Busp() {
        name = "busp";
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/res/objects/busp.png"));

        }catch(IOException e) {
            e.printStackTrace();
        }
    }

}
