package object;

import java.io.IOException;
import javax.imageio.ImageIO;

public class ItauBike extends SuperObject {
    
    public ItauBike() {
        name = "itauBike";
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/res/objects/itauBike.png"));

        }catch(IOException e) {
            e.printStackTrace();
        }
        collision = true;
    }
}
