package object;

import java.io.IOException;
import javax.imageio.ImageIO;

public class Pokebola extends SuperObject {
    
    public Pokebola() {
        name = "pokebola";
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/res/objects/pokebola.png"));

        }catch(IOException e) {
            e.printStackTrace();
        }
    }

}
