package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.IOException;

import javax.swing.JPanel;

import entity.Player;
import entity.Entity;

import object.SuperObject;
import tiles.TileManager;

public class GamePanel extends JPanel implements Runnable {

    // Screen settings
    final int originalTileSize = 16; // 16X16 Tile size
    final int scale = 3; // Scale to Tile size

    public final int tileSize = originalTileSize * scale;
    public final int maxScreenCol = 16;
    public final int maxScreenRow = 12;
    public final int screenWidth = tileSize * maxScreenCol; // 768 pixels
    public final int screenHeight = tileSize * maxScreenRow; // 576 pixels

    // World settings
    public final int maxWorldCol = 66;
    public final int maxWorldRow = 66;
    public final int worldWidth = tileSize * maxWorldCol;
    public final int worldHeight = tileSize * maxWorldRow;

    public final int maxMap = 3;
    public int currentMap = 0; // mapa atual

    // FPS
    int FPS = 60;

    // entity
    public Entity npc[][] = new Entity[maxMap][10];
    public Entity polimon[][] = new Entity[maxMap][10];
    private Entity entity = new Entity(this);
    public Battle battle;

    // System
    public KeyHandler keyH = new KeyHandler(this);
    TileManager tileM = new TileManager(this, keyH);
    public AssetSetter aSetter = new AssetSetter(this, entity);
    Sound sound = new Sound();
    Thread gameThread;
    public CollisionChecker cChecker = new CollisionChecker(this);
    public WaterChecker wChecker = new WaterChecker(this);
    public EventHandler eHandler = new EventHandler(this);
    public String dificult;

    // objects
    public Player player = new Player(this, keyH);
    public SuperObject obj[][] = new SuperObject[maxMap][10]; // podem ser colocados até 10 objetos ao memso tempo na //
                                                              // tela
    public setObjects setterObj = new setObjects(this);

    // mensagens que aparecem na tela do jogo
    public ScreenMessages sMessages = new ScreenMessages(this);

    // Estados do jogo
    public int gameState;
    public final int titleState = 0;
    public final int playState = 1;
    public final int pauseState = 2;
    public final int dialogueState = 3;
    public final int inventoryState = 4;

    public final int battleState = 5;

    public GamePanel() {
        this.setPreferredSize((new Dimension(screenWidth, screenHeight)));
        this.setBackground(Color.white);
        this.setDoubleBuffered(true);
        this.addKeyListener(keyH);
        this.setFocusable(true);
    }

    // coloca os objetos no mapa antes do jogo começar
    public void setupGame() {
        setterObj.setObject();
        aSetter.setNPC();
        aSetter.setPolimon();
        gameState = titleState;
        playMusic(0);
        playMusic(4);
    }

    public void startGameThread() {
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {

        double drawInterval = 1000000000 / FPS; // 0.01666 seconds
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;
        long timer = 0;
        int drawCount = 0;

        while (gameThread != null) {

            currentTime = System.nanoTime();
            delta += (currentTime - lastTime) / drawInterval;
            timer += (currentTime - lastTime);

            lastTime = currentTime;

            if (delta >= 1) {
                try {
                    update();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                repaint();
                delta--;
                drawCount++;
            }

            if (timer >= 1000000000) {

                drawCount = 0;
                timer = 0;
            }
        }
    }

    public void update() throws InterruptedException {

        // Modo play(playstate)
        if (gameState == playState) {
            player.update();
            for (int i = 0; i < npc[1].length; i++) {
                if (npc[currentMap][i] != null) {
                    npc[currentMap][i].update();
                }
            }
            for (int i = 0; i < polimon[1].length; i++) {
                if (polimon[currentMap][i] != null) {
                    polimon[currentMap][i].update();
                }
            }
        }

        // Modo pause(pausestate)
        if (gameState == pauseState) {
            if (keyH.dificult)
                dificult = "DIFÍCIL";
            else
                dificult = "FÁCIL";
        }

        // Modo batalha
        if (gameState == battleState) {
            battle.Attack();
            battle.CheckWin(this);
        }

        // player
        tileM.updateSkinMap();

    }

    public void paintComponent(Graphics g) {

        super.paintComponent(g); // JPanel is super
        Graphics2D g2 = (Graphics2D) g;

        // TELA INICIAL
        if (gameState == titleState) {
            try {
                sMessages.draw(g2);
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        else {

            // tile
            tileM.desenhar(g2);

            // objects
            for (int i = 0; i < obj[1].length; i++) {
                if (obj[currentMap][i] != null)
                    obj[currentMap][i].draw(g2, this);
            }

            // NPC
            for (int i = 0; i < npc[1].length; i++) {

                if (npc[currentMap][i] != null) {
                    npc[currentMap][i].draw(g2);
                }
            }

            // POLIMON
            for (int i = 0; i < polimon[1].length; i++) {

                if (polimon[currentMap][i] != null) {
                    polimon[currentMap][i].draw(g2);
                }
            }

            // player
            player.draw(g2);

            // mensagens, textos na tela do usuario
            try {
                sMessages.draw(g2);
            } catch (IOException e) {
                e.printStackTrace();
            }

            g2.dispose();

        }

    }

    public void playMusic(int i) {

        sound.setFile(i);
        sound.play();
        sound.loop();

    }

    public void stopMusic() {

        sound.stop();

    }

    public void playSE(int i) {

        sound.setFile(i);
        sound.play();

    }
}
