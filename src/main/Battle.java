package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import entity.Entity;

public class Battle {
    public Entity player;
    public Entity polimon;
    public int playerDamage;
    public int polimonDamage;
    public int playerLife;
    public int polimonLife;
    BufferedImage battlemap, heart;

    public boolean turn; // true = turno do player // false = turno do oponente

    public Battle(Entity player, Entity polimon) {
        this.player = player;
        this.polimon = polimon;
        playerDamage = 0;
        polimonDamage = 0;
        turn = true;
        try {
            battlemap = ImageIO.read(getClass().getResourceAsStream("/res/battle/battlemap.jpg"));
            heart = ImageIO.read(getClass().getResourceAsStream("/res/battle/heart.png"));

        } catch (IOException e) {
            e.printStackTrace();
        }
        playerLife = player.energia;
        if (player.gp.dificult == "FÁCIL")
            polimonLife = polimon.energia;
        else
            polimonLife = polimon.energia + 1;
    }

    public void Attack() throws InterruptedException {
        if (turn) {
            if (player.gp.keyH.enterPressed) {
                polimonDamage++;
                turn = false;
            }
        } else {
            TimeUnit.SECONDS.sleep(1);
            playerDamage++;
            turn = true;
        }
    }

    public void CheckWin(GamePanel gp) throws InterruptedException {
        if (polimonDamage == polimonLife) {
            String text = "PARABÉNS! VOÇÊ DERROTOU O POLIMÓN!";
            int textX = gp.sMessages.getXforCenteredText(text);
            gp.sMessages.g2.setFont(gp.sMessages.g2.getFont().deriveFont(Font.BOLD, 25F));
            gp.sMessages.g2.setColor(Color.orange);
            gp.sMessages.g2.drawString(text, textX, 3 * gp.tileSize);
            TimeUnit.SECONDS.sleep(2);
            gp.gameState = gp.playState;
            gp.polimon[gp.currentMap][gp.player.polimonIndex] = null;
            player.energia += 1;
        }
        // else if (playerLife == 0)
        // return 2;
        // else
        // return 0;
    }

}
