package main;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sound {

    Clip clip;
    File soundURL[] = new File[30];

    public Sound(){

        soundURL[0] = new File("./src/res/sound/streets.wav");
        soundURL[1] = new File("./src/res/sound/busp.wav");
        soundURL[2] = new File("./src/res/sound/bike.wav");
        soundURL[3] = new File("./src/res/sound/catraca.wav");
        soundURL[4] = new File("./src/res/sound/fantasy.wav");
        soundURL[5] = new File("./src/res/sound/error.wav");
        soundURL[6] = new File("./src/res/sound/foot.wav");

    }
    
    public void setFile(int i) {

        try {

            AudioInputStream ais = AudioSystem.getAudioInputStream(soundURL[i]);
            clip = AudioSystem.getClip();
            clip.open(ais);

        } catch(Exception e){

        }
    }

    public void play(){

        clip.start();

    }

    public void loop(){

        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop(){

        clip.stop();

    }
}
