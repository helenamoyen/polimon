package main;

import entity.Entity;

public class WaterChecker {

    GamePanel gp;

    public WaterChecker(GamePanel gp) {
        this.gp = gp;
    }

    public void checkTile(Entity entity) {

        int entityLeftWorldX = entity.worldX;
        int entityRightWorldX = entity.worldX + gp.tileSize;
        int entityTopWorldY = entity.worldY;
        int entityBottomWorldY = entity.worldY + gp.tileSize;

        int entityLeftCol = entityLeftWorldX / gp.tileSize;
        int entityRightCol = entityRightWorldX / gp.tileSize;
        int entityTopRow = entityTopWorldY / gp.tileSize;
        int entityBottomRow = entityBottomWorldY / gp.tileSize;

        int tileNum1, tileNum2;

        switch (entity.direction) {
            case "up":
                entityTopRow = (entityTopWorldY - entity.speed) / gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityTopRow][entityLeftCol][gp.currentMap];
                tileNum2 = gp.tileM.mapTileNum[entityTopRow][entityRightCol][gp.currentMap];

                if (gp.tileM.tiles[tileNum1].water == true || gp.tileM.tiles[tileNum2].water == true) {
                    entity.waterOn = true;
                }
                break;
            case "down":
                entityBottomRow = (entityBottomWorldY + entity.speed) / gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityBottomRow][entityLeftCol][gp.currentMap];
                tileNum2 = gp.tileM.mapTileNum[entityBottomRow][entityRightCol][gp.currentMap];

                if (gp.tileM.tiles[tileNum1].water == true || gp.tileM.tiles[tileNum2].water == true) {
                    entity.waterOn = true;
                }

                break;
            case "left":
                entityLeftCol = (entityLeftWorldX - entity.speed) / gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityTopRow][entityLeftCol][gp.currentMap];
                tileNum2 = gp.tileM.mapTileNum[entityBottomRow][entityLeftCol][gp.currentMap];

                if (gp.tileM.tiles[tileNum1].water == true || gp.tileM.tiles[tileNum2].water == true) {
                    entity.waterOn = true;
                }
                break;
            case "right":
                entityRightCol = (entityRightWorldX + entity.speed) / gp.tileSize;
                tileNum1 = gp.tileM.mapTileNum[entityTopRow][entityRightCol][gp.currentMap];
                tileNum2 = gp.tileM.mapTileNum[entityBottomRow][entityRightCol][gp.currentMap];

                if (gp.tileM.tiles[tileNum1].water == true || gp.tileM.tiles[tileNum2].water == true) {
                    entity.waterOn = true;
                }
                break;
        }
    }
}
