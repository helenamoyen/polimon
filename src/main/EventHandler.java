package main;

public class EventHandler {

    GamePanel gp;
    EventRect eventRect[][][];

    // aqui é para ver se caso um evento aconteça, não acontece de novo até ele sair
    // do retangulo do evento e voltar, assim não fica acontecendo o evento
    // repetidamente
    int previusEventX, previusEventY;
    boolean canTouchEvent = true;

    public EventHandler(GamePanel gp) {
        this.gp = gp;

        eventRect = new EventRect[gp.maxMap][gp.maxWorldCol][gp.maxWorldRow];

        int map = 0;
        int col = 0;
        int row = 0;
        while (map < gp.maxMap && col < gp.maxWorldCol && row < gp.maxWorldRow) {

            eventRect[map][col][row] = new EventRect();
            eventRect[map][col][row].x = 23;
            eventRect[map][col][row].y = 23;
            eventRect[map][col][row].width = 2;
            eventRect[map][col][row].height = 2;
            eventRect[map][col][row].eventRectDefaultX = eventRect[map][col][row].x;
            eventRect[map][col][row].eventRectDefaultY = eventRect[map][col][row].y;

            col++;
            if (col == gp.maxWorldCol) {
                col = 0;
                row++;

                if (row == gp.maxWorldRow) {
                    row = 0;
                    map++;
                }
            }
        }
    }

    public void checkEvent() {

        // checa se o player está fora do retangulo de último evento ocorrido
        int xDistance = Math.abs(gp.player.worldX - previusEventX);
        int yDistance = Math.abs(gp.player.worldY - previusEventY);
        int distance = Math.max(xDistance, yDistance);
        if (distance > gp.tileSize)
            canTouchEvent = true;

        if (canTouchEvent) {
            // passagem entre os mapas 1 e 2
            if (hit(0, 26, 64, "any") == true)
                teleportMaps(1, 15, 41);
            else if (hit(1, 15, 41, "any") == true)
                teleportMaps(0, 26, 64);
        }

    }

    public boolean hit(int map, int col, int row, String reqDirection) {

        boolean hit = false;

        if (map == gp.currentMap) {

            // pegando a area atual do player
            gp.player.solidArea.x = gp.player.worldX + gp.player.solidArea.x;
            gp.player.solidArea.y = gp.player.worldY + gp.player.solidArea.y;
            // pegando a area em que deve acontecer o evento
            eventRect[map][col][row].x = col * gp.tileSize + eventRect[map][col][row].x;
            eventRect[map][col][row].y = row * gp.tileSize + eventRect[map][col][row].y;

            // checa se o player está interagindo com essa area do evento em qualquer
            // direção
            if (gp.player.solidArea.intersects(eventRect[map][col][row])) {
                if (gp.player.direction.contentEquals(reqDirection) || reqDirection.contentEquals("any")) {
                    hit = true;

                    previusEventX = gp.player.worldX;
                    previusEventY = gp.player.worldY;
                }
            }
            gp.player.solidArea.x = gp.player.solidAreaDefaultX;
            gp.player.solidArea.y = gp.player.solidAreaDefaultY;
            eventRect[map][col][row].x = eventRect[map][col][row].eventRectDefaultX;
            eventRect[map][col][row].y = eventRect[map][col][row].eventRectDefaultY;
        }

        return hit;
    }

    // seta as coordenadas do mapa e o mapa onde quer q ocorra essa transição/
    // mudança de mapa
    public void teleportMaps(int map, int col, int row) {

        gp.currentMap = map;
        gp.player.worldX = gp.tileSize * col;
        gp.player.worldY = gp.tileSize * row;
        previusEventX = gp.player.worldX;
        previusEventY = gp.player.worldY;
        canTouchEvent = false;
    }
}
